//
//  CachedDocumentController.swift
//  j131.tk
//
//  Created by Daniel on 16/3/23.
//  Copyright © 2016年 Daniel. All rights reserved.
//

import UIKit

class CachedDocumentController: UITableViewController, UIDocumentInteractionControllerDelegate {
    var fileCache:FileCache = FileCache();

    override func viewDidLoad() {
        super.viewDidLoad();
        self.clearsSelectionOnViewWillAppear = false;
        self.title = "查看缓存";
        self.tableView.register(UINib(nibName: "ItemCell", bundle: nil), forCellReuseIdentifier: "ItemCell");
        self.tableView.register(ItemCell.self, forCellReuseIdentifier: "itemCellClass");
        self.tableView.rowHeight = 53;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIDocumentInteractionController Delegate Function
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self.navigationController!;
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileCache.fileCount();
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell;
        cell.loadImage(false);
        cell.itemName?.text = fileCache[indexPath.row];
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView!.deselectRow(at: indexPath, animated: true);
        self.previewFile(FileCache.read("/"+fileCache[indexPath.row])!);
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */
    
    func previewFile(_ localURL: URL) {
        let docInteractionVC = UIDocumentInteractionController(url: localURL);
        docInteractionVC.delegate = self;
        self.tabBarController!.tabBar.isHidden = true;
        let otherCanOpen:Bool = docInteractionVC.presentPreview(animated: true);
        if (!otherCanOpen) {
            self.showError("无法打开文件", message: "没有对应的应用程序可以打开此文件！");
        }
    }
    
    func showError(_ title: String, message: String) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title: "确定", style: UIAlertActionStyle.cancel, handler: nil));
        self.present(alert, animated: true, completion: nil);
        self.tabBarController?.tabBar.isHidden = false;
    }
}
