//
//  ApiRequest.swift
//  6ccloud.com
//
//  Created by Daniel on 15/10/29.
//  Copyright © 2015年 Daniel. All rights reserved.
//

import Foundation
import UIKit
class ApiRequest {
    static let LIST_COUNT = "https://api.6ccloud.com/api/listCount";
    static let LIST_FOLDER = "https://api.6ccloud.com/api/listFolder";
    static let FILE_CHECKSUM = "https://api.6ccloud.com/api/fileChecksum";
    static func get(_ url: String, params: Dictionary<String, String>?) -> AnyObject? {
        if (Reach().connectionStatus() == ReachabilityStatus.offline) {
            return nil;
        }
        let requestURL = url + self.createGetParam(params);
        var httpResult:Data?;
        do {
            httpResult = try HttpRequest.get(requestURL);
            let json = try JSONSerialization.jsonObject(with: httpResult!, options: []);
            return json as AnyObject?;
        } catch {
            return nil;
        }
    }
    static func getAsync(_ url:String, params: Dictionary<String, String>?, completionHandler:@escaping (_ response: URLResponse?, _ data: Data?, _ error: NSError?) -> Void) {
        let requestURL = url + self.createGetParam(params);
        HttpRequest.getAsync(requestURL, completionHandler: completionHandler);
    }
    
    static fileprivate func createGetParam(_ params: Dictionary<String, String>?) -> String {
        if (params == nil) {
            return "";
        }
        var url = "?";
        for (key, value) in params! {
            url += "\(key)=\(value)&";
        }
        url = url.substring(to: url.characters.index(url.endIndex, offsetBy: -1));
        return url;
    }
}
