//
//  SettingController.swift
//  j131.tk
//
//  Created by Daniel on 16/3/4.
//  Copyright © 2016年 Daniel. All rights reserved.
//

import UIKit

class SettingController: UITableViewController {
    let greyBgColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0);
    var actions:Dictionary<Int, Dictionary<Int, Array<Any>>> = [
        0 : [
            0 : ["查看缓存", showCache],
        ],
        1 : [
            0 : ["清除全部缓存", clearCache],
        ]
    ];
    
    override func viewDidLoad() {
        self.tableView.backgroundColor = self.greyBgColor;
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return actions.count;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actions[section]!.count;
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "";
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = self.greyBgColor;
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section != actions.count-1) {
            return nil;
        }
        let view = UIView();
        view.backgroundColor = self.greyBgColor;
        return view;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: nil);
        cell.textLabel?.text = actions[indexPath.section]![indexPath.row]![0] as? String;
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator;
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true);
        let function = actions[indexPath.section]![indexPath.row]![1] as! (SettingController)->()->();
        function(self)();
    }
    
    func showCache() {
        self.navigationController!.pushViewController(CachedDocumentController(), animated: true);
    }
    
    func clearCache() {
        let size = FileCache.clearAll();
        let alert = UIAlertController.init(title: "清除缓存", message: "清除了\(size)缓存！", preferredStyle: UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title: "确定", style: UIAlertActionStyle.cancel, handler: nil));
        self.present(alert, animated: true, completion: nil);
    }
}
