//
//  HttpRequest.swift
//  j131.tk
//
//  Created by Daniel on 15/10/28.
//  Copyright © 2015年 邱家辉. All rights reserved.
//

import Foundation
import UIKit

class HttpRequest {
    static func get(_ url: String) throws -> Data? {
        let request = URLRequest(url: URL(string: url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!);
        var data: Data?;
        try data = NSURLConnection.sendSynchronousRequest(request, returning: nil);
        return data;
    }
    
    static func getAsync(_ url: String, completionHandler: @escaping (_ response: URLResponse?, _ data: Data?, _ error: NSError?) -> Void)
    {
        let request = URLRequest(url: URL(string: url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!);
        let queue = OperationQueue.main;
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler: completionHandler as! (URLResponse?, Data?, Error?) -> Void);
    }
    
    static func getURL(_ url: String) -> URL {
        return URL(string: url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!;
    }
}
