//
//  ItemCell.swift
//  j131.tk
//
//  Created by Daniel on 16/3/2.
//  Copyright © 2016年 Daniel. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var cancelButton: UIButton!
    fileprivate var cancelButtonDownEventHandler: () -> Void;
    
    required init?(coder aDecoder: NSCoder) {
        self.cancelButtonDownEventHandler = {};
        super.init(coder: aDecoder);
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.progressView.isHidden = true;
        self.cancelButton.isHidden = true;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func loadImage(_ isFolder: Bool) {
        self.itemImage.image = UIImage(named: isFolder ? "folderImg" : "fileImg");
    }
    
    func registerCancelButtonDownEvent(_ handler: @escaping () -> Void) {
        self.cancelButtonDownEventHandler = handler;
    }
    
    @IBAction func cancelButtonDown(_ sender: AnyObject) {
        cancelButtonDownEventHandler();
    }
    
    func startDownload() {
        self.progressView.progress = 0;
        self.progressView.isHidden = false;
        self.cancelButton.isHidden = false;
    }
    
    func endDownload() {
        self.progressView.progress = 0;
        self.progressView.isHidden = true;
        self.cancelButton.isHidden = true;
    }
}
